package eu.fasola.fragmentyzadanie;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDataInput2 extends Fragment implements SeekBar.OnSeekBarChangeListener {

    String[] carnivourus = {"Dromeozaury", "Tyranozaury", "Raptory", "Ornitozaury"};
    SeekBar seekBar;
    TextView weight;
    Button add;
    Button reset;
    Spinner carniSpinner;
    EditText nameText;
    EditText foundPlace;
    RatingBar rating;

    public FragmentDataInput2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_data_input2, container, false);
        carniSpinner = view.findViewById(R.id.spinnerCarnivorous);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, carnivourus);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        carniSpinner.setAdapter(adapter);
        weight = view.findViewById(R.id.textWeight);
        seekBar = view.findViewById(R.id.seekBarWeight);
        add = view.findViewById(R.id.buttonAdd);
        reset = view.findViewById(R.id.buttonReset);
        nameText = view.findViewById(R.id.textName);
        rating = view.findViewById(R.id.ratingBar);
        nameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameText.setText("");
            }
        });
        foundPlace = view.findViewById(R.id.textFoundPlace);
        foundPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foundPlace.setText("");
            }
        });
        seekBar.setOnSeekBarChangeListener(this);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetMethod();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMethod();
            }
        });

        return view;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        weight.setText(new Integer(progress).toString());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void resetMethod() {
        carniSpinner.setSelection(0);
        seekBar.setProgress(3500);
        nameText.setText("");
        foundPlace.setText("");
        rating.setRating(0);
    }

    private void addMethod() {
    }
}
