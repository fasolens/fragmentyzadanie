package eu.fasola.fragmentyzadanie;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentData extends Fragment
//        implements FragmentDataSelect.OnWyborOpcjiListener {
{

//    FragmentDataSelect fragmentDataSelect;
    FragmentDataInput fragmentDataInput;
//    FragmentDataInput2 fragmentDataInput2;
    FragmentTransaction transaction;
//
//    private static final String TAG_FDataSelect = "FragmentDataSelect";
    private static final String TAG_FDataInput = "FragmentDataInput";
//    private static final String TAG_FDataInput2 = "FragmentDataInput2";

    public FragmentData() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState == null) {
//            fragmentDataSelect = new FragmentDataSelect();
            fragmentDataInput = new FragmentDataInput();
//            fragmentDataInput2 = new FragmentDataInput2();
//
            transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.dataInputContainer, fragmentDataInput, TAG_FDataInput);
//            transaction.detach(fragmentDataInput);
//            transaction.add(R.id.dataInputContainer, fragmentDataInput2, TAG_FDataInput2);
//            transaction.detach(fragmentDataInput2);
            transaction.commit();
        } else {
            fragmentDataInput = (FragmentDataInput) getChildFragmentManager().findFragmentByTag(TAG_FDataInput);
//            fragmentDataInput2 = (FragmentDataInput2) getChildFragmentManager().findFragmentByTag(TAG_FDataInput2);
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_data, container, false);
    }

//    @Override
//    public void onWyborOpcji(int opcja) {
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        switch (opcja) {
//            case 0:
//                transaction.detach(fragmentDataInput2);
//                transaction.attach(fragmentDataInput);
//                break;
//            case 1:
//                transaction.detach(fragmentDataInput);
//                transaction.attach(fragmentDataInput2);
//                break;
//        }
//        transaction.commit();
//    }
}
