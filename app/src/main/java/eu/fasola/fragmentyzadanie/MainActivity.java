package eu.fasola.fragmentyzadanie;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import eu.fasola.fragmentyzadanie.dino.Dino;

public class MainActivity extends AppCompatActivity
        implements ActionBar.TabListener,
        FragmentDataSelect.OnWyborOpcjiListener,
        CarnivousFragment.OnListFragmentInteractionListener,
        HerbivoreFragment.OnListFragmentInteractionListener {

    FragmentInfo fragmentInfo;

    FragmentData fragmentData;
    FragmentDataSelect fragmentDataSelect;
    FragmentDataInput fragmentDataInput;
    FragmentDataInput2 fragmentDataInput2;

    CarnivousFragment carnivousFragment;
    HerbivoreFragment herbivoreFragment;

    FragmentTransaction transaction;

    private static final String TAG_FInfo = "FragmentInfo";
    private static final String TAG_FData = "FragmentData";
    private static final String TAG_FDataSelect = "FragmentDataSelect";
    private static final String TAG_FDataInput = "FragmentDataInput";
    private static final String TAG_FDataInput2 = "FragmentDataInput2";
    private static final String TAG_FCarnivous = "CarnivousFragment";
    private static final String TAG_FHerbivore = "HerbivoreFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            fragmentInfo = new FragmentInfo();

            fragmentData = new FragmentData();
            fragmentDataSelect = new FragmentDataSelect();
            fragmentDataInput = new FragmentDataInput();
            fragmentDataInput2 = new FragmentDataInput2();

            carnivousFragment = new CarnivousFragment();
            herbivoreFragment = new HerbivoreFragment();

            transaction = getSupportFragmentManager().beginTransaction();

            transaction.add(R.id.mainContainer, fragmentInfo, TAG_FInfo);
            transaction.detach(fragmentInfo);

            transaction.add(R.id.mainContainer, fragmentData, TAG_FData);
            transaction.detach(fragmentData);

            transaction.add(R.id.mainContainer, carnivousFragment, TAG_FCarnivous);
            transaction.detach(carnivousFragment);

            transaction.add(R.id.mainContainer, herbivoreFragment, TAG_FHerbivore);
            transaction.detach(herbivoreFragment);

            transaction.commit();

            ActionBar actionBar = getSupportActionBar();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            ActionBar.Tab tab;

            tab = actionBar.newTab().setText("Info").setTabListener(this);
            actionBar.addTab(tab);

            tab = actionBar.newTab().setText("Input").setTabListener(this);
            actionBar.addTab(tab);

            tab = actionBar.newTab().setText("Carnivourus").setTabListener(this);
            actionBar.addTab(tab);

            tab = actionBar.newTab().setText("Herbivores").setTabListener(this);
            actionBar.addTab(tab);
        } else {
            fragmentInfo = (FragmentInfo) getSupportFragmentManager().findFragmentByTag(TAG_FInfo);
            fragmentData = (FragmentData) getSupportFragmentManager().findFragmentByTag(TAG_FData);
            carnivousFragment = (CarnivousFragment) getSupportFragmentManager().findFragmentByTag(TAG_FCarnivous);
            herbivoreFragment = (HerbivoreFragment) getSupportFragmentManager().findFragmentByTag(TAG_FHerbivore);
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        int opcja = tab.getPosition();
        switch (opcja) {
            case 0:
                ft.attach(fragmentInfo);
                break;
            case 1:
//                FragmentTransaction FrTr = getSupportFragmentManager().beginTransaction();
//                FrTr.add(R.id.dataInputContainer, fragmentDataInput, TAG_FDataInput);
////                FrTr.detach(fragmentDataInput);
//                FrTr.add(R.id.dataInputContainer, fragmentDataInput2, TAG_FDataInput2);
//                FrTr.detach(fragmentDataInput2);
//                FrTr.commit();
                ft.attach(fragmentData);
                break;
            case 2:
                ft.attach(carnivousFragment);
                break;
            case 3:
                ft.attach(herbivoreFragment);
                break;
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        int opcja = tab.getPosition();
        switch (opcja) {
            case 0:
                ft.detach(fragmentInfo);
                break;
            case 1:
                ft.detach(fragmentData);
                break;
            case 2:
                ft.detach(carnivousFragment);
                break;
            case 3:
                ft.detach(herbivoreFragment);
                break;
        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onWyborOpcji(int opcja) {
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        switch (opcja) {
//            case 0:
//                transaction.detach(fragmentDataInput2);
//                transaction.attach(fragmentDataInput);
//                break;
//            case 1:
//                transaction.detach(fragmentDataInput);
//                transaction.attach(fragmentDataInput2);
//                break;
//        }
//        transaction.commit();
    }

    @Override
    public void onListFragmentInteraction(Dino item) {

    }
}
