package eu.fasola.fragmentyzadanie;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDataSelect extends Fragment implements RadioGroup.OnCheckedChangeListener {

    AppCompatActivity A1;
    OnWyborOpcjiListener sluchaczA1;

    public FragmentDataSelect() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_data_select, container, false);
    }

    public interface OnWyborOpcjiListener {
        void onWyborOpcji(int opcja);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            A1 = (AppCompatActivity) context;
            sluchaczA1 = (OnWyborOpcjiListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(A1.toString() +
                    " musi implementować OnWyborOpcjiListener");
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radioDinosaur:
                sluchaczA1.onWyborOpcji(0);
                break;
            case R.id.radioReptile:
                sluchaczA1.onWyborOpcji(1);
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((RadioGroup) getActivity().findViewById(R.id.dataSelectRadio)).setOnCheckedChangeListener(this);
    }
}
