package eu.fasola.fragmentyzadanie.dino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DinoContent {
    public static final List<Dino> ITEMS = new ArrayList<>();

    public static final Map<String, Dino> ITEM_MAP = new HashMap<>();

    private static final int COUNT = 5;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Dino item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.name, item);
    }

    private static Dino createDummyItem(int position) {
        return new Dino("Zauropody",
                "Brachiozaur" + position,
                "Colorado",
                46+position, "ton");
    }

//    private static String makeDetails(int position) {
//        StringBuilder builder = new StringBuilder();
//        builder.append("Details about Item: ").append(position);
//        for (int i = 0; i < position; i++) {
//            builder.append("\nMore details information here.");
//        }
//        return builder.toString();
//    }
}
