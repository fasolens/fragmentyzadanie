package eu.fasola.fragmentyzadanie.dino;

public class Dino {
    public final String family;
    public final String name;
    public final String found;
    public final int weight;
    public final String weightUnit;
    public final String era;

    public Dino(String family,
                String name,
                String found,
                int weight,
                String weightUnit) {
        this.family = family;
        this.name = name;
        this.found = found;
        this.weight = weight;
        this.weightUnit = weightUnit;
        this.era = "Mesozoic";
    }

    @Override
    public String toString() {
        return name + " waga: "  + weight + " " + weightUnit;
    }
}
