package eu.fasola.fragmentyzadanie;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentInfo extends Fragment {

    TextView currentData;

    public FragmentInfo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_info, container, false);
        currentData = view.findViewById(R.id.currentData);
        Time now = new Time();
        now.setToNow();
        currentData.setText(now.year + "-" + now.month + "-" + now.monthDay);
        // Inflate the layout for this fragment
        return view;
    }

}
