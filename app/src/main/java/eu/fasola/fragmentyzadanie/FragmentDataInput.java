package eu.fasola.fragmentyzadanie;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDataInput extends Fragment implements SeekBar.OnSeekBarChangeListener {

    String[] herbivore = {"Zauropody", "Ankylozaury", "Ceratopsy", "Heterodontozaury"};
    SeekBar seekBar;
    TextView weight;
    Button add;
    Button reset;
    Spinner herbSpinner;
    EditText nameText;
    EditText foundPlace;
    RatingBar rating;

    public FragmentDataInput() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_data_input, container, false);
        herbSpinner = view.findViewById(R.id.spinnerHerbivore);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, herbivore);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        herbSpinner.setAdapter(adapter);
        weight = view.findViewById(R.id.textWeight);
        seekBar = view.findViewById(R.id.seekBarWeight);
        add = view.findViewById(R.id.button2);
        reset = view.findViewById(R.id.button3);
        nameText = view.findViewById(R.id.textName);
        rating = view.findViewById(R.id.ratingBar);
        nameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameText.setText("");
            }
        });
        foundPlace = view.findViewById(R.id.textFoundPlace);
        foundPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                foundPlace.setText("");
            }
        });
        seekBar.setOnSeekBarChangeListener(this);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetMethod();
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMethod(v);
            }
        });
        return view;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        weight.setText(new Integer(progress).toString());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void resetMethod() {
        herbSpinner.setSelection(0);
        seekBar.setProgress(12);
        nameText.setText("");
        foundPlace.setText("");
        rating.setRating(0);
    }

    public void addMethod(View view) {
        String name = nameText.getText().toString();
        String found = foundPlace.getText().toString();
        String weightString = weight.toString();
        String like = rating.toString();
    }
}
